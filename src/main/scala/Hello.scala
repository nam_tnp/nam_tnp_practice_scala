import java.util.regex.Pattern
import scala.io.StdIn.{readFloat, readInt}


object Hello {
  def main(args: Array[String]) = {
    //task1: Convert C to F
    var c = readFloat();
    task1(c)
    //task2: Convert C to F
    task2(c)
    //Task3: Using the input value 2.7255, generate the string “You owe $2.73.”
    var x = readFloat();
    task3(x)
    //Task4 :Is there a simpler way to write the following?
        //val flag: Boolean = false
        //val result: Boolean = (flag == false)

    val flag: Boolean = true;
    //Task5: Convert the number 128 to a Char, a String, a Double, and then back to an Int.
    var y = readInt();
    task5(y)

    //Task6: Using the input string “Frank,123 Main,925-555-1943,95122” and regular expression matching, retrieve the telephone number
    task6("Frank,123 Main,925-555-1943,95122");
    task7("1")
    //Task8: Given a double amount, write an expression to return “greater” if it is more than zero, “same” if it equals zero, and “less” if it is less than zero
    println(task8(-0.999))
    //Task10 : Print the numbers 1 to 100, with each line containing a group of five numbers
    task10()
    //Task11: Write an expression to print the numbers from 1 to 100, except that for multiples of 3, print “type,” and for multiples of 5, print “safe.” For multiples of both 3 and 5, print “typesafe.”
    task11()

  }

  //task1: Convert C to F
  def task1( c: Float  ) = {
    var f:Float = c * 9 /5 + 32
    println("Độ F Float: " + f );
  }

  //task2: Convert C to F
  def task2( c: Float  ) = {
    var f = ((c * 9 /5) + 32).toInt
    println("Độ F Int: " + f );
  }

  //Task3: Using the input value 2.7255, generate the string “You owe $2.73.”

  def task3(x: Float): Unit ={
    var str = String.format("%.2f", x)
    println("You owe $" + str)
  }

  //Task5: Convert the number 128 to a Char, a String, a Double, and then back to an Int.
  def task5(x: Int): Unit ={
    var char = x.toChar
    var string = x.toString
    var double = x.toDouble

    println(char)
    println(string)
    println(double)
  }


  //task6:
  def task6(x: String): Unit ={
    val pattern = Pattern.compile("\\d{3}-\\d{3}-\\d{4}")
    val matcher = pattern.matcher(x)
    if (matcher.find) {
      var parts = (matcher.group(0)).toString
      val arr = parts.split("-")
      for (str <- arr) {
        println(str)
      }
    }
  }

  //task7
  def task7(str: String): Unit ={
    str match {
      case str if !str.isEmpty => println(str)
      case _ => println("n/a")
    }
  }
  //task8
  def task8(x: Double): String ={
    x match {
      case x if x > 0 => "greater"
      case x if x < 0 => "less"
      case _ => "same"
    }
  }

  //task10
  def task10(): Unit ={
    var a=0
    for( a <- 1 to 100){
      print( a +",");
      if(a%5==0) print("\n")
    }
  }
  //task11
  def task11(): Unit ={
    var a=0
    for( a <- 1 to 100){
      a match {
        case a if (a % 15 == 0) => println("typesafe")
        case a if (a % 3 == 0) => println("type")
        case a if (a % 5 == 0) => println("safe")
        case _ => println(a)
      }
    }
  }
}